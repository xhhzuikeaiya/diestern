using System;
using System.Collections.Generic;
using Code.Scripts.UI.Inventory;
using UnityEngine;

namespace Code.Scripts.InventorySystem
{
    public class Inventory : Singleton<Inventory>
    {
        public InventoryPanel panel;
        private ItemDatabase _itemDatabase;

        public Dictionary<string, int> Items { private set; get; }

        public void AddItem(string title, int amount = 1)
        {
            if (!panel) return;
            var item = _itemDatabase.FindItemByTitle(title);
            if (item == null) return;
            Items[item.title] += panel.AddItem(item, amount);
        }

        public void RemoveItem(string title, int amount = 1)
        {
            if (!panel) return;
            var item = _itemDatabase.FindItemByTitle(title);
            if (item == null) return;
            Items[item.title] -= panel.RemoveItem(item, amount);
            if (Items[item.title] <= 0) Items.Remove(item.title);
        }
        
        public void InitPanel()
        {
            if (!panel) return;
            panel.Clear();
            foreach (var itemAndCount in Items)
            {
                var item = _itemDatabase.FindItemByTitle(itemAndCount.Key);
                var count = itemAndCount.Value;

                panel.AddItem(item, count);
            }
        }

        public void Clear()
        {
            PlayerPrefs.DeleteKey(PlayerPrefsKey);
            Items.Clear();
        }

        public string ToJson()
        {
            return JsonUtility.ToJson(Items);
        }

        public void FromJson(string json)
        {
            Items = JsonUtility.FromJson<Dictionary<string, int>>(json);
        }

        public const string PlayerPrefsKey = "Inventory";
        public void Save()
        {
            PlayerPrefs.SetString(PlayerPrefsKey, ToJson());
        }

        private void Awake()
        {
            _itemDatabase = Resources.Load<ItemDatabase>("InventorySystem/ItemDatabase");
            if(ArchiveManager.Instance.ArchiveExisted)
            {
                if (PlayerPrefs.HasKey(PlayerPrefsKey))
                {
                    var json = PlayerPrefs.GetString("Inventory");
                    FromJson(json);
                    if (Items != null) return;
                }
            }
            
            Items = new Dictionary<string, int>();
        }

        private void Update()
        {
            panel = InventoryPanel.ActivePanel;
            if (panel != null && !panel.hasInit)
            {
                InitPanel();
                panel.hasInit = true;
            }
        }
    }
}