using Code.Scripts.Chapters;
using Code.Scripts.EventBook;
using Code.Scripts.InventorySystem;
using UnityEngine;

namespace Code.Scripts
{
    public class ArchiveManager : Singleton<ArchiveManager>
    {
        public bool ArchiveExisted => 
            GameProgressManager.Instance.HasGameProgressStored;

        public void Save()
        {
            GameProgressManager.Instance.Save();
            Inventory.Instance.Save();
            EventBookManager.Instance.Save();
            PlayerPrefs.Save();
        }

        protected override void OnDestroyOrQuit()
        {
            Save();
        }

        public void NewGame()
        {
            GameProgressManager.Instance.Clear();
            Inventory.Instance.Clear();
            EventBookManager.Instance.Clear();
            
            PlayerPrefs.Save();
        }
    }
}