using System.Collections.Generic;
using UnityEngine;

namespace Code.Scripts.Conversations
{
    [CreateAssetMenu(fileName = "NewChat", menuName = "Conversations/Chat", order = 0)]
    public class Chat : ScriptableObject
    {
        public string title;
        public List<Message> messages;
    }
}