using UnityEngine;

namespace Code.Scripts.Conversations
{
    [CreateAssetMenu(fileName = "NewPerson", menuName = "Conversations/Person", order = 0)]
    public class Person : ScriptableObject
    {
        public Sprite avatar;
        public new string name;
    }
}