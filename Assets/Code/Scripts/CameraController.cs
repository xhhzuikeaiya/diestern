using UnityEngine;

namespace Code.Scripts
{
    public class CameraController : MonoBehaviour
    {
        private GameObject _character;

        private Vector3 _offset;
        private void Update()
        {
            if (!_character)
            {
                _character = CharacterController.ActiveController.gameObject;
                if (!_character)
                {
                    _character = null;
                    return;
                }
                _offset = transform.position - _character.transform.position;
            }

            if (!_character || PauseManager.Instance.IsPaused) return;
            transform.position = _character.transform.position + _offset;
        }
    }
}