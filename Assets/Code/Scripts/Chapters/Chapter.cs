using System.Collections.Generic;
using Code.Scripts.UI;
using Code.Scripts.UI.NotOver;
using UnityEngine;

namespace Code.Scripts.Chapters
{
    [ExecuteAlways]
    public class Chapter : MonoBehaviour
    {
        public string title;
        public List<Room> rooms;

        private int _currentIndex;

        private Dictionary<string, int> RoomIndexDictionary
        {
            get
            {
                var dic = new Dictionary<string, int>();
                for (var i = 0; i < rooms.Count; i++) dic[rooms[i].title] = i;
                return dic;
            }
        }

        public void Reset()
        {
            EnterRoom(0);
        }

        private void Update()
        {
            name = title;
            
            var notOverCanvas = NotOverCanvas.ActiveNotOverCanvas;
            if (notOverCanvas == null) return;
            if (!notOverCanvas.showing || !notOverCanvas.end) return;
            GameStatusManager.Instance.status = GameStatusManager.Status.InMainMenu;
            ArchiveManager.Instance.NewGame();
        }

        private void OnDisable()
        {
            CurrentActiveRoom().gameObject.SetActive(false);
            _currentIndex = 0;
        }

        public void EnterRoom(string roomTitle)
        {
            if (!RoomIndexDictionary.ContainsKey(roomTitle)) return;
            EnterRoom(RoomIndexDictionary[roomTitle]);
        }

        private void EnterRoom(int index)
        {
            CurrentActiveRoom().gameObject.SetActive(false);
            rooms[index].gameObject.SetActive(true);
            _currentIndex = index;
        }

        public Room CurrentActiveRoom()
        {
            return rooms[_currentIndex];
        }

        public void NextChapter()
        {
            if (GameProgressManager.Instance.AdvanceProgress()) return;
            var notOverCanvas = NotOverCanvas.ActiveNotOverCanvas;
            if (notOverCanvas == null) return;
            notOverCanvas.Show();
        }
    }
}