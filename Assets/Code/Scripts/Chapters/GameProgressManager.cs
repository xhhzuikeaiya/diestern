using System.Collections.Generic;
using Code.Scripts.InventorySystem;
using UnityEngine;

namespace Code.Scripts.Chapters
{
    public class GameProgressManager : Singleton<GameProgressManager>
    {
        public const int InvalidChapterIndex = -1;

        public const string PlayerPrefsKey = "GameProgress";

        private ChapterDatabase _chapterDatabase;

        private Dictionary<string, GameObject> _chapterGameObjectCache;

        public bool IsChapterDatabaseEmpty => _chapterDatabase.chapters.Count == 0;

        public int CurrentChapter { get; private set; } = InvalidChapterIndex;

        public bool HasGameProgressStored => PlayerPrefs.HasKey(PlayerPrefsKey) &&
                                             IsValidIndex(PlayerPrefs.GetInt(PlayerPrefsKey));

        private void Awake()
        {
            _chapterDatabase = Resources.Load<ChapterDatabase>("Chapters/ChapterDatabase");
            if (PlayerPrefs.HasKey(PlayerPrefsKey))
            {
                var index = PlayerPrefs.GetInt(PlayerPrefsKey);
                if (!IsValidIndex(index)) index = InvalidChapterIndex;
                CurrentChapter = index;
            }
            else
            {
                Save();
            }
        }

        private void Start()
        {
            _chapterGameObjectCache = new Dictionary<string, GameObject>();
            var chapterObjects = Resources.FindObjectsOfTypeAll<Chapter>();
            foreach (var chapterObject in chapterObjects)
                _chapterGameObjectCache[chapterObject.name] = chapterObject.gameObject;
        }

        private bool IsValidIndex(int index)
        {
            return index != InvalidChapterIndex && index < _chapterDatabase.chapters.Count - 1;
        }

        public void Save()
        {
            PlayerPrefs.SetInt(PlayerPrefsKey, CurrentChapter);
        }

        public bool AdvanceProgress()
        {
            if (_chapterDatabase.chapters.Count - 1 == CurrentChapter)
                return false;
            Inventory.Instance.Save();
            Save();
            CurrentChapter++;
            return true;
        }

        public string CurrentChapterName()
        {
            return _chapterDatabase.chapters[CurrentChapter];
        }

        public GameObject CurrentChapterGameObject()
        {
            var chapterName = CurrentChapterName();
            return _chapterGameObjectCache[chapterName];
        }

        public void Clear()
        {
            PlayerPrefs.DeleteKey(PlayerPrefsKey);
            CurrentChapter = InvalidChapterIndex;
            Save();
        }
    }
}