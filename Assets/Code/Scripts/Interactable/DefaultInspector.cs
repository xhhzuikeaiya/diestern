using UnityEngine;

namespace Code.Scripts.Interactable
{
    public class DefaultInspector : MonoBehaviour, IInspector
    {
        public void Inspect()
        {
        }

        public bool Collected { get; } = false;
    }
}