using System;
using Code.Scripts.InventorySystem;
using UnityEngine;

namespace Code.Scripts.Interactable
{
    public class Interactable : MonoBehaviour
    {
        private IInspector Inspector => GetComponent<IInspector>();
        private SpriteRenderer SpriteRenderer => GetComponent<SpriteRenderer>();

        private static GameObject Character => CharacterController.ActiveController.gameObject;

        public float thresholdDistance = 10.0f;

        private bool ClosedToPlayer =>
            Vector3.Distance(transform.position, Character.transform.position) <= thresholdDistance;

        private Material _defaultMaterial;
        public Material highlightMaterial;

        private Inventory _inventory;
        
        private void Start()
        {
            _inventory = Inventory.Instance;
            _defaultMaterial = SpriteRenderer.material;
            _inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        }

        private bool _highlighted = false;

        // private static Inventory Inventory => Singleton<Inventory>.Instance;
        private Item Item => GetComponent<Item>();
        
        private void Update()
        {
            if (Inspector !=null && Inspector.Collected)
            {
                if (Item && _inventory)
                {
                    _inventory.AddItem(Item.title);
                }
                gameObject.transform.SetParent(null);
                Destroy(gameObject);
                return;
            }

            if (thresholdDistance < 0)
            {
                HighLight();
                return;
            }
            if (ClosedToPlayer)
            {
                if(_highlighted) return;
                HighLight();
            }
            else
            {
                Unhighlight();
            }
        }

        private void HighLight()
        {
            _highlighted = true;
            SpriteRenderer.material = highlightMaterial;
        }
        
        private void Unhighlight()
        {
            _highlighted = false;
            SpriteRenderer.material = _defaultMaterial;
        }

        private void OnMouseDown() => Inspector.Inspect();

        private void OnMouseEnter()
        {
            HighLight();
        }

        private void OnMouseExit()
        {
            Unhighlight();
        }
    }
}