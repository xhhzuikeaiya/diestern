using Code.Scripts.Chapters;
using UnityEngine;

namespace Code.Scripts
{
    public class GameStatusManager : Singleton<GameStatusManager>
    {
        public enum Status
        {
            InMainMenu,
            Gaming
        }

        public Status status = Status.InMainMenu;
        private GameObject _activeObject;
        private GameObject _inGameUI;
        private GameObject _mainMenu;
        private GameObject _mainMenuCamera;
        private GameObject _chapterCamera;
        
        private void Start()
        {
            _mainMenu = GameObject.Find("MainMenu");
            _inGameUI = GameObject.Find("InGameUI");
            _mainMenuCamera = GameObject.Find("MainMenuCamera");
            _chapterCamera = GameObject.Find("ChapterCamera");
        }

        private void Update()
        {
            switch (status)
            {
                case Status.InMainMenu:
                    if(_activeObject != _mainMenu)
                    {
                        ClearActiveObject();
                        _inGameUI.SetActive(false);
                        _activeObject = _mainMenu;
                        _mainMenu.SetActive(true);
                        _mainMenuCamera.SetActive(true);
                        _chapterCamera.SetActive(false);
                    }
                    break;
                case Status.Gaming:
                    if (GameProgressManager.Instance.IsChapterDatabaseEmpty)
                    {
                        status = Status.InMainMenu;
                        return;
                    }
                    UpdateGameProgress();
                    break;
            }
        }

        private void ClearActiveObject()
        {
            if (_activeObject != null)
            {
                ArchiveManager.Instance.Save();
                _activeObject.SetActive(false);
                _activeObject = null;
            }
        }

        private void UpdateGameProgress()
        {
            var chapterObject =
                GameProgressManager.Instance.CurrentChapterGameObject();
            if (chapterObject == _activeObject) return;
            ArchiveManager.Instance.Save();
            ClearActiveObject();
            _activeObject = chapterObject;
            chapterObject.SetActive(true);
            _mainMenuCamera.SetActive(false);
            _chapterCamera.SetActive(true);
            _inGameUI.SetActive(true);
        }

        protected override void OnDestroyOrQuit()
        {
            ArchiveManager.Instance.Save();
        }
    }
}