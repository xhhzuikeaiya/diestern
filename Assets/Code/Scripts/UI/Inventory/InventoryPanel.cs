using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Code.Scripts.UI.Inventory
{
    public class InventoryPanel : MonoBehaviour
    {
        public static InventoryPanel ActivePanel;
        public GameObject tooltipObject;
        public GameObject deleteConfirmObject;

        public bool hasInit;
        private Collapsed _collapsed;
        private GameObject _collapsedObject;
        private DeleteConfirm _deleteConfirm;
        private Full _full;

        private GameObject _fullObject;
        private List<ItemData> _itemDataObjects;
        private Tooltip _tooltip;

        public void Clear()
        {
            foreach (var itemData in _itemDataObjects)
            {
                itemData.Clear();
            }
        }
        
        private void Start()
        {
            Init();
        }

        private void Update()
        {
            foreach (var itemData in _itemDataObjects.Where(itemData => !itemData.IsEmpty()))
            {
                if (itemData.ShouldClear)
                {
                    itemData.Clear();
                    continue;
                }

                if (itemData.ShouldShowTooltip)
                {
                    _tooltip.ItemData = itemData;
                    _tooltip.aliveCounter++;
                    continue;
                }

                if (itemData.ShouldDisplayDeleteConfirm)
                {
                    _deleteConfirm.ToDelete = itemData;
                    _deleteConfirm.gameObject.SetActive(true);
                    break;
                }
            }

            if (Input.GetKeyDown("e")) _full.Toggle();
        }

        private void OnEnable()
        {
            ActivePanel = this;
        }

        private void OnDisable()
        {
            if (ActivePanel == this) ActivePanel = null;
            hasInit = false;
        }

        private void Init()
        {
            _fullObject = transform.Find("Full").gameObject;
            _full = _fullObject.GetComponent<Full>();
            _collapsedObject = transform.Find("Collapsed").gameObject;
            _collapsed = _collapsedObject.GetComponent<Collapsed>();

            _itemDataObjects = _collapsed.GetAllItemDataObject();
            _itemDataObjects.AddRange(_full.GetAllItemDataObject());

            _deleteConfirm = deleteConfirmObject.GetComponent<DeleteConfirm>();
            _tooltip = tooltipObject.GetComponent<Tooltip>();
        }

        public int AddItem(InventorySystem.Item item, int amount = 1)
        {
            if (item.stackable)
            {
                foreach (var itemData in
                    _itemDataObjects.Where(itemData => itemData.Item == item))
                {
                    itemData.Count += amount;
                    return amount;
                }

                foreach (var itemData in
                    _itemDataObjects.Where(itemData => itemData.IsEmpty()))
                {
                    itemData.Item = item;
                    itemData.Count = amount;
                    return amount;
                }

                return 0;
            }

            var i = 0;
            for (; i < amount; i++)
            {
                var added = false;
                foreach (var itemData in
                    _itemDataObjects.Where(itemData => itemData.IsEmpty()))
                {
                    itemData.Item = item;
                    itemData.Count = 1;
                    added = true;
                    break;
                }

                if (!added) break;
            }

            return i;
        }

        public int RemoveItem(InventorySystem.Item item, int amount = 1)
        {
            if (item.stackable)
            {
                foreach (var itemData in
                    _itemDataObjects.Where(itemData => itemData.Item == item))
                {
                    var toReturn = itemData.Count;
                    itemData.Count -= amount;
                    if (itemData.Count <= 0) itemData.Clear();
                    else toReturn = amount;
                    return toReturn;
                }

                return 0;
            }

            var i = 0;
            for (; i < amount; i++)
            {
                var removed = false;
                foreach (var itemData in
                    _itemDataObjects.Where(itemData => itemData.Item == item))
                {
                    removed = true;
                    itemData.Clear();
                }

                if (!removed) break;
            }

            return i;
        }
    }
}