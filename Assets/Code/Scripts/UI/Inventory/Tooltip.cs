using System;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Scripts.UI.Inventory
{
    public class Tooltip : MonoBehaviour
    {
        public ItemData ItemData;
        public int aliveCounter = 100;
        
        public Text title;
        public Image icon;
        public Text description;

        private RectTransform _rect;

        private void Start()
        {
            _rect = GetComponent<RectTransform>();
        }

        private void Update()
        {
            _rect.position = Input.mousePosition;
            
            if (aliveCounter < 0 || ItemData == null)
            {
                aliveCounter = 10;
                ItemData = null;
                gameObject.SetActive(false);
                return;
            }

            aliveCounter--;

            title.text = ItemData.Title;
            icon.sprite = ItemData.Icon;
            description.text = ItemData.Description;
        }
    }
}