using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Scripts.UI.Inventory
{
    public class Collapsed : MonoBehaviour
    {
        private List<Item> _items;

        private void Start()
        {
            _items = new List<Item>(5);
            for (var i = 0; i < 5; i++)
            {
                _items.Add(
                    transform.
                    Find($"Item{i+1}").
                    gameObject.
                    GetComponent<Item>());
                _items[i].selected = false;
            }

            _items[0].selected = true;
        }

        private int _lastSelected = 0;
        
        private void Update()
        {
            SelectItem();
        }

        private void SelectItem()
        {
            for (var i = 0; i < 5; i++)
            {
                if (!Input.GetKeyDown($"{i + 1}")) continue;
                if (_lastSelected >= 0)
                {
                    _items[_lastSelected].selected = false;
                }
                _items[i].selected = true;
                _lastSelected = i;
                break;
            }
        }

        private int AvailableItem()
        {
            for (var i = 0; i < 5; i++)
            {
                if (_items[i].IsEmpty())
                {
                    return i;
                }
            }

            return -1;
        }
        
        public bool PlaceItem(ItemData data)
        {
            var index = AvailableItem();
            if (index < 0)
            {
                return false;
            }

            _items[index].ItemData = data;
            return true;
        }

        public List<ItemData> GetAllItemDataObject()
        {
            var objects = new List<ItemData>(5);
            for (var i = 0; i < 5; i++)
            {
                objects.Add(_items[i].ItemData);
            }

            return objects;
        }
    }
}