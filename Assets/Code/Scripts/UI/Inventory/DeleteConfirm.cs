using System;
using UnityEngine;

namespace Code.Scripts.UI.Inventory
{
    public class DeleteConfirm : MonoBehaviour
    {
        public ItemData ToDelete;
        public WindowManager window;

        private void OnEnable()
        {
            window.OpenWindow();
        }

        private void OnDisable()
        {
            ToDelete.ShouldDisplayDeleteConfirm = false;
            ToDelete = null;
        }

        public void Confirm()
        {
            ToDelete.ShouldClear = true;
            window.CloseWindow();
        }

        public void Cancel()
        {
            ToDelete.ShouldClear = false;
            window.CloseWindow();
        }
    }
}