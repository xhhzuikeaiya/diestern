using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Code.Scripts.UI.Inventory
{
    public class Full : MonoBehaviour
    {
        private List<Item> _items;
        private WindowManager _window;

        private void Start()
        {
            var items = GetComponentsInChildren<Item>();
            _items = items.ToList();
            _window = GetComponent<WindowManager>();
        }
        
        private int AvailableItem()
        {
            for (var i = 0; i < _items.Count; i++)
            {
                if (_items[i].IsEmpty())
                {
                    return i;
                }
            }

            return -1;
        }
        
        public bool PlaceItem(ItemData data)
        {
            var index = AvailableItem();
            if (index < 0)
            {
                return false;
            }

            _items[index].ItemData = data;
            return true;
        }

        public void Toggle()
        {
            _window.Toggle();
        }
        
        public List<ItemData> GetAllItemDataObject()
        {
            var objects = new List<ItemData>(5);
            objects.AddRange(_items.Select(t => t.ItemData));
            return objects;
        }
    }
}