using System;
using Code.Scripts.Conversations;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Scripts.UI.Conversations
{
    public class MessageCanvas : MonoBehaviour
    {
        public Message defaultMessage;
        public Message message;

        public Image avatarImage;
        public Text nameText;
        public Text messageText;

        public bool shouldShowNext;

        public GameObject displayArea;

        private void Update()
        {
            ShowMessage(message ? message : defaultMessage);
        }

        public void Show()
        {
            displayArea.SetActive(true);
        }

        public void Hide()
        {
            displayArea.SetActive(false);
        }

        private void ShowMessage(Message msg)
        {
            avatarImage.sprite = msg.from.avatar;
            avatarImage.preserveAspect = true;
            nameText.text = msg.from.name;
            messageText.text = msg.message;
        }
    }
}