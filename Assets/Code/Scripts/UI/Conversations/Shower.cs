using System.Collections.Generic;
using Code.Scripts.Conversations;
using UnityEngine;

namespace Code.Scripts.UI.Conversations
{
    public class Shower : MonoBehaviour
    {
        public static Shower ActiveShower;

        public MessageCanvas canvas;

        private List<Message> _toShow;

        private void Update()
        {
            UpdateCanvas();
        }

        private void OnEnable()
        {
            ActiveShower = this;
        }

        private void OnDisable()
        {
            if (ActiveShower == this) ActiveShower = null;
        }

        public void Show(Message message)
        {
            _toShow = new List<Message> {message};
            canvas.shouldShowNext = true;
            UpdateCanvas();
        }
        
        public void Show(Chat chat)
        {
            _toShow = chat.messages.GetRange(0, chat.messages.Count);
            if (_toShow.Count == 0)
                return;
            canvas.shouldShowNext = true;
            UpdateCanvas();
        }

        private void UpdateCanvas()
        {
            if (_toShow == null || _toShow.Count == 0 && canvas.shouldShowNext)
            {
                canvas.message = null;
                PauseManager.Instance.Unpause();
                if (Controller.Controller.ActiveController)
                {
                    Controller.Controller.ActiveController.Show();
                }
                canvas.Hide();
                _toShow = null;
                return;
            }

            if (!canvas.shouldShowNext) return;
            // WindowManager.ShouldCloseAll = true;
            PauseManager.Instance.Pause();
            if (Controller.Controller.ActiveController)
            {
                Controller.Controller.ActiveController.Hide();
            }
            canvas.shouldShowNext = false;
            canvas.message = _toShow[0];
            _toShow.RemoveAt(0);
            canvas.Show();
        }
    }
}