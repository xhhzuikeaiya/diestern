using UnityEngine;

namespace Code.Scripts.UI.PauseMenu
{
    public class HandlePause : MonoBehaviour
    {
        public GameObject pauseBackground;
        private WindowManager _window;

        private void Start()
        {
            _window = GetComponent<WindowManager>();
            _window.onWindowOpen.AddListener(OnWindowOpen);
            _window.onWindowClosed.AddListener(OnWindowClosed);
            pauseBackground.SetActive(false);
        }

        private void OnWindowClosed()
        {
            pauseBackground.SetActive(false);
            PauseManager.Instance.Unpause();
        }

        private void OnWindowOpen()
        {
            pauseBackground.SetActive(true);
            PauseManager.Instance.Pause();
        }
    }
}