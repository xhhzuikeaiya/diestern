using UnityEngine;

namespace Code.Scripts.UI.MainMenu
{
    public class MainMenuManager : MonoBehaviour
    {
        public GameObject continueGameButton;

        private void Start()
        {
            continueGameButton.SetActive(false);
            if (ArchiveManager.Instance.ArchiveExisted) continueGameButton.SetActive(true);
        }

        public void NewGame()
        {
            ArchiveManager.Instance.NewGame();
            GameStatusManager.Instance.status = GameStatusManager.Status.Gaming;
        }

        public void ContinueGame()
        {
            GameStatusManager.Instance.status = GameStatusManager.Status.Gaming;
        }
    }
}