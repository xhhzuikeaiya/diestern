using UnityEngine;

namespace Code.Scripts.UI.Stuff
{
    [ExecuteAlways]
    public class Bubble : MonoBehaviour
    {
        [Range(0, 100)] public float percentage;

        private RectTransform _fillRect;

        private void Start()
        {
            _fillRect = transform.Find("Fill").GetComponent<RectTransform>();
        }
    }
}