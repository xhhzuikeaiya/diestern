using UnityEngine;
using UnityEngine.EventSystems;

namespace Code.Scripts.UI
{
    public class Holdable : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        public int max = 150;
        public int pressing;

        private void Update()
        {
            if (pressing > 0 && pressing < max)
            {
                Debug.Log($"pressing: {pressing}");
                pressing++;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            pressing = 1;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            pressing = 0;
        }
    }
}