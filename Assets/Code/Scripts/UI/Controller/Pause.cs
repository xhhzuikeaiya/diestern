using UnityEngine;

namespace Code.Scripts.UI.Controller
{
    public class Pause : MonoBehaviour
    {
        public WindowManager pauseWindow;

        public void TogglePause()
        {
            PauseManager.Instance.Toggle();
            pauseWindow.Toggle();
        }
    }
}